# Створіть ієрархію класів транспортних засобів. У загальному класі опишіть загальні всім транспортних засобів поля,
# у спадкоємцях – специфічні їм. Створіть кілька екземплярів. Виведіть інформацію щодо кожного транспортного засобу.

class Vehicles:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year

    def vehicle_info(self):
        print(f"Make - {self.make}, Model - {self.model}, Year - {self.year}")


class Car(Vehicles):
    def __init__(self, make, model, year, num_doors):
        super().__init__(make, model, year)
        self.num_doors = num_doors

    def vehicle_info(self):
        super().vehicle_info()
        print(f"Number of doors: {self.num_doors}")


class Motorcycle(Vehicles):
    def __init__(self, make, model, year, engine_size):
        super().__init__(make, model, year)
        self.engine_size = engine_size

    def vehicle_info(self):
        super().vehicle_info()
        print(f"Engine size: {self.engine_size} cc")


car1 = Car("Mercedes-Benz", "E-Class", 2017, 4)
motorcycle1 = Motorcycle("Harley-Davidson", "Sportster", 2021, 1200)

car1.vehicle_info()
motorcycle1.vehicle_info()

