# Опишіть класи графічного об'єкта, прямокутника та об'єкта, який може обробляти натискання миші. Опишіть клас кнопки.
# Створіть об'єкт кнопки та звичайного прямокутника. Викличте метод натискання на кнопку.


class GraphicObject:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def object_display(self):
        print(f"Object by {self.x} and {self.y} coordinates, with width = {self.width} and height = {self.height}.")


class Rectangle(GraphicObject):
    def __init__(self, x, y, width, height, color):
        super().__init__(x, y, width, height)
        self.color = color

    def object_display(self):
        super().object_display()
        print(f"The color of the rectangle is {self.color}")


class MouseClicksObject:
    def click_process(self, x, y):
        print(f"Clicking the mouse at the point ({x}, {y}).")


class MouseButton(Rectangle, MouseClicksObject):
    def __init__(self, x, y, width, height, color):
        super().__init__(x, y, width, height, color)

    def press(self):
        self.click_process(self.x, self.y)


button = MouseButton(5, 7, 60, 30, 'black')
rectangle = Rectangle(25, 25, 75, 55, 'red')

button.press()
