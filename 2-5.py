# Використовуючи код example_10, створіть декоратор @staticmethod для визначення повноліття людини в Україні та Америки


from datetime import date


class MyClass1:
    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def from_birth_year(cls, surname, name, birth_year):
        return cls(surname, name, date.today().year - birth_year)

    @staticmethod
    def is_adult(age, country):
        if country == "Ukraine":
            return age >= 18
        elif country == "America":
            return age >= 21
        else:
            raise ValueError("Unsupported country")

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))


class MyClass2(MyClass1):
    color = 'White'


m_per1 = MyClass1('Ivanenko', 'Ivan', 19)
m_per2 = MyClass1.from_birth_year('Dovzhenko', 'Bogdan',  2000)
m_per3 = MyClass2.from_birth_year('Sydorchuk', 'Petro', 2010)
m_per4 = MyClass2.from_birth_year('Makuschenko', 'Dmytro', 2001)

m_per1.print_info()
m_per2.print_info()

print(f"Is {m_per1.surname} {m_per1.name} of legal age in Ukraine?", MyClass1.is_adult(m_per1.age, "Ukraine"))
print(f"Is {m_per2.surname} {m_per2.name} of legal age in America?", MyClass1.is_adult(m_per2.age, "America"))
