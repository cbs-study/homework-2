# Створіть ієрархію класів із використанням множинного успадкування. Виведіть на екран порядок вирішення методів
# для кожного класу. Поясніть, чому лінеаризація даних класів виглядає саме так.


class Human:
    def talk(self):
        print("Hello! I am Victor.")


class Monster:
    def growl(self):
        print("Grrr! I am a monster.")


class Frankenstein(Human, Monster):
    pass


print(Frankenstein.__mro__)

frankenstein = Frankenstein()
frankenstein.talk()
frankenstein.growl()
