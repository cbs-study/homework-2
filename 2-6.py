# Використовуючи код example_10, створіть декоратори @classmethod для формування переліку об'єктів,
# які підрахують кількість повнолітніх людей в Україні та Америці.

from datetime import date

class MyClass1:
    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def from_birth_year(cls, surname, name, birth_year):
        return cls(surname, name, date.today().year - birth_year)

    @classmethod
    def count_adults(cls, people):
        count = sum(1 for person in people if person.age >= 18)
        return count

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))


class MyClass2(MyClass1):
    color = 'White'


m_per1 = MyClass1('Ivanenko', 'Ivan', 19)
m_per2 = MyClass1.from_birth_year('Dovzhenko', 'Bogdan',  2000)
m_per3 = MyClass2.from_birth_year('Sydorchuk', 'Petro', 2010)
m_per4 = MyClass2.from_birth_year('Makuschenko', 'Dmytro', 2001)

m_per1.print_info()
m_per2.print_info()

ukrainian = [m_per1, m_per2]
american = [m_per3, m_per4]

print("Number of adults from Ukraine:", MyClass1.count_adults(ukrainian))
print("Number of adults from America:", MyClass1.count_adults(american))
