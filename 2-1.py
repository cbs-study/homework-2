# Завдання 1
# Створіть клас Editor, який містить методи view_document та edit_document. Нехай метод edit_document виводить на екран
# інформацію про те, що редагування документів недоступне для безкоштовної версії. Створіть підклас ProEditor, у якому
# цей метод буде перевизначено. Введіть ліцензійний ключ із клавіатури і, якщо він коректний,
# створіть екземпляр класу ProEditor, інакше Editor. Викликайте методи перегляду та редагування документів.


class Editor:
    def view_document(self):
        print("Document editing is available.")

    def edit_document(self):
        print("Document editing is not available for the free version.")


class ProEditor(Editor):
    def __init__(self, license_key):
        self.license_key = license_key

    def edit_document(self):
        if self.license_key == "valid_key":
            print("Document editing is available for the Pro version.")
        else:
            super().edit_document()


license_key = input("Enter the license key: ")

if license_key == "valid_key":
    editor = ProEditor(license_key)
else:
    editor = Editor()

editor.view_document()
editor.edit_document()
